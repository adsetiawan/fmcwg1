%read TARA
fid=fopen('..\REF\TARA\wag_noised50_20060720102041.dat','r');
hdr=fread(fid,784,'uchar');
N_cell=1024;

%start reading data
N_bin=512;
s=zeros(N_bin,N_cell);
S=s; %prepare for range spectrum data
for m=1:N_bin;
    data=fread(fid,N_cell,'int16');
    s(m,1:N_cell) = data(1:N_cell) ; %
end;
%=fft(data);
fclose(fid);
%x_axis=1:floor(N_cell/2);

%plot a beat
figure(1);plot(s(1,:));
title('beat frequency');

% plot spectrum of s
for m=1:N_bin;
    S(m,1:N_cell)=fft(s(m,1:N_cell)); 
end;
S_MAG=abs(S);
figure(2); 
imagesc(S_MAG(:, 1:floor(N_cell/2)));
title('Range Spectrum');

% now calculate Range-Doppler
for m=1:N_bin;
    S_dopp(m,1:N_cell)=fft(S(m,1:N_cell)); 
end;

S_MAG_DOPP=abs(S_dopp);
figure(3); 
imagesc(S_MAG_DOPP(:, 1:floor(N_cell/2)));
title('Doppler Spectrum');

figure(4);
plot(sum(S_MAG_DOPP(:, 1:floor(N_cell/2))));
