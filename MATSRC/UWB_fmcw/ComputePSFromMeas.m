%% Compute Point Spread Function H(f) from Measurement Using NA
%% Equivalent to Simulation with an ideal sample delay
%% with system effects included using NA measured files.
% Load Models
% Sample Radar Parameters.
function [PSF] = ComputePSFromMeas(radarParams);
% Sampling Parameters ...
nT = radarParams(2);
tS = radarParams(3);
fR = 1/(nT*tS);
tAxis = 0:tS:(nT-1)*tS;
72
f0 = radarParams(5);
f1 = radarParams(7);
BW = f1-f0;
fc = (f0+f1)/2; % Center Frequency.
fAxis = (0:fR:(nT/2)*fR)';
freq_Axis = [flipud(-fAxis(2:end));fAxis(1:end-1)];
[Wv] = GenerateChirpWaveform(radarParams);
Tx_Wv = SysPass(Wv,nT,tS,'Tx.txt');
Rx_LO = SysPass(Wv,nT,tS,'TxLo.txt');
Rx = [zeros(2000,1);Tx_Wv(1:end-2000)];
Rx_Wv = SysPass(Rx,nT,tS,'Rx.txt');
Mix_Wv = Rx_Wv.*Rx_LO;
Rx_Mix = Mix_Wv;
Num_Fft = 2^21;
freq_Axis_range = max(freq_Axis) +fR - min(freq_Axis);
freq_Axis_res = freq_Axis_range/Num_Fft;
freq_Axis_fft = [-freq_Axis_range/2:freq_Axis_res:freq_Axis_range/2-
freq_Axis_res];
freq_Axis_pos = freq_Axis_fft(end/2+1:end);
Range_Prof = fftshift(fft(Rx_Mix,Num_Fft));
Calib_indx = find(Range_Prof(end/2+1:end) ==
max(Range_Prof(end/2+1:end)));
F_calib = freq_Axis_pos(Calib_indx);
Filter_Wn = [Calib_indx-100 Calib_indx+100]/Num_Fft;
[Filt_B,Filt_A] = butter(2,Filter_Wn);
[Filt_H,Filt_W] = freqz(Filt_B,Filt_A,Num_Fft);
Calib_data_filt_f = ifftshift(Range_Prof).*abs(Filt_H);
Calib_data_filt = real(ifft(Calib_data_filt_f'));
Calib_data_filt_m = hilbert(Calib_data_filt(end:-1:end-nT+1));
System_m = abs(Calib_data_filt_m);
System_p = abs(unwrap(angle(Calib_data_filt_m))) -
2*pi*F_calib*tAxis;
PSF = System_m.*exp(j*System_p);
%% %% Plot Tranfer Function
%% figure;
%% plot(linspace(2e9,8e9,nT),20*log10(System_m));
%% xlabel('Freq -->');
%% ylabel('Magnitde (dB)-->');
%% grid;
%% title('Point Spread Function from Measuremnt using NA');
return;