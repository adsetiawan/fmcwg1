function [dProf] = ComputeDvsT(radarParams, erLayer, Depth)
Epsilon0 = 1e-9/(36*pi);
Mu0 = (4e-7)*pi;
nLayers = length(erLayer(1,:));
% Sampling Parameters ...
nT = radarParams(2);
tS = radarParams(3);
74
tAxis = 0:tS:(nT-1)*tS;
% Compute the Speed of Light in each layer.
Vel = real(1./sqrt(mean(erLayer)*Epsilon0*Mu0));
Vel = [Vel,Vel(end)];
% Generate Cumulative Depths!!
CumDepth = [cumsum(Depth);Inf];
% Computing the Distance Axis.
indx1 = 1;
d_cur = 0;
dProf = zeros(size(tAxis));
for indx2 = 2:nT
d_cur = d_cur + Vel(indx1).*tS/2;
if(d_cur > CumDepth(indx1))
indx1 = indx1 + 1;
if (indx1 > nLayers)
break;
end;
end;
dProf(indx2) = d_cur;
end;
dProf(indx2:end) = d_cur + Vel(indx1).*(tAxis(indx2:end)-
tAxis(indx2-1))/2;
return;