%% AMPM Model
%% Determines Amplitude and Phase Error Models
function [PMModel,AMModel] = ampm_model(NoArg);
%% Freq_Volt_Pow.txt is the Modeling Measurement
X=load('Freq_Volt_Pow.txt');
f=X(:,1);% - 20e6;
v=X(:,2);
PdBm = X(:,3);
%% AMPM.s2p charachterizes the AMPM Setup.
data = load('AMPM.s2p');
fr = data(:,1);
s_db = data(:,4);
s_m = 10.^(s_db/20);
s_a = data(:,5);
[s_r,s_i] = pol2cart(s_a*pi/180,s_m);
S21 = s_r + j*s_i;
t_end = 10e-3;
t_0 = 0;
m=(t_end-t_0)/(v(end)-v(1));
t=t_0+m*(v-v(1));
f_t = polyfit(t,f,1);
ff_t = polyval(f_t,t);
ff_ideal = f(1) + (f(end) - f(1))/t_end*t;
f_end = 8e9;
f_0 = 2e9;
phi_ideal = 2*pi*(f_0*t + 0.5*(f_end - f_0)/t_end*t.^2);
phi_t = 2*pi*polyint(f_t);
phi = polyval(phi_t,t);
PMModel = phi_t;
68
Phse = interp1(f,phi,fr,'','extrap');
R = 50;
P = 10.^((PdBm-30)/10); % Power in watts
A = sqrt(P*R); % Amplitude in volts.
Amp = interp1(f,A,fr,'','extrap');
Amp_Setup = abs(Amp./S21);
%A_Setup = interp1(fr,Amp_Setup,f,'','extrap');
%A_DC = A_Setup - mean(A_Setup);
Amp_DC = Amp_Setup - mean(Amp_Setup);
Amp_Comps = dct(Amp_DC);
%% Compute DCT freq. axis;
dct_t_ax = linspace(0,.01,401);
dct_Fs = 1/(dct_t_ax(2)-dct_t_ax(1));
dct_ax = linspace(0,dct_Fs/2,length(Amp_DC));
Amp_Comps1 = zeros(size(Amp_Comps));
%Amp_Comps1(1:100) = Amp_Comps(1:100);
%Significant_Components_Index = [5,9,11,16,20,49,50,73];
Significant_Components_Index = find(abs(Amp_Comps) > 0.1);
Amp_Comps1(Significant_Components_Index) =
Amp_Comps(Significant_Components_Index);
Amp_DC1 = idct(Amp_Comps1);
AMModel = Amp_Comps1;