--------- %
% Compute Distance Axis
% -------------------------
% Computed the Distance Axis
%
% Paramters :
% -----------
% radarParams -> The type of the radar used affects the velocity profile.
% erLayer -> Dielectric Information too affects the velocity profile.
% Depth -> Depth profile of the geo-physical data.
%
% ------------------------------------------------------------------
--------- %
function [dProfile] = ComputeDProfile(radarParams, erLayer,
Depth,fAxis)
Epsilon0 = 1e-9/(36*pi);
69
Mu0 = (4e-7)*pi;
nLayers = length(erLayer(1,:));
% Sampling Parameters ...
nT = radarParams(2);
tS = radarParams(3);
fR = 1/(nT*tS);
f0 = radarParams(5);
f1 = radarParams(7);
BW = f1-f0;
% Compute the Speed of Light in each layer.
Vel = real(1./sqrt(mean(erLayer)*Epsilon0*Mu0));
Vel = [Vel,Vel(end)];
% Generate Cumulative Depths!!
CumDepth = [cumsum(Depth);Inf];
% Computing the Distance Axis.
disp('Compute Distance Axis...');
indx1 = 1;
d_cur = 0;
dProfile = zeros(size(fAxis));
fStep = fAxis(2)-fAxis(1);
for indx2 = 2:length(fAxis)
d_cur = d_cur + Vel(indx1).*fStep/(2*fR*BW);
if(d_cur > CumDepth(indx1))
indx1 = indx1 + 1;
if (indx1 > nLayers)
break;
end;
end;
dProfile(indx2) = d_cur;
end;
dProfile(indx2:end) = d_cur + Vel(indx1).*(fAxis(indx2:end)-
fAxis(indx2-1))./(2*fR*BW);
return;