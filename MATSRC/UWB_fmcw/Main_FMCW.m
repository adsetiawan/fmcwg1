clear;clc;close all;
addpath('../Data');
addpath('../Library');
radarParams = [1, 50000, 5e-11, 0, 2e9, 10e-3, 8e9];
win_type = 'hanning';
geoData = load('air_snow_seaice.csv');
Sig_h_1 = 0.009; % Standard deviation of height (Snow)
L_c_1 = 0.5; % Correlation Length (Snow)
Sig_h_2 = 0.007; % Standard deviation of height (Sea Ice) (2 to 5cm)
L_c_2 = 0.02; % Correlation Length (Sea Ice) (40 cm) (2 to 90cm)
% Sampling Parameters ...
nT = radarParams(2);
tS = radarParams(3);
fS = 1/tS;
fR = 1/(nT*tS);
tAxis = 0:tS:(nT-1)*tS;
f0 = radarParams(5);
f1 = radarParams(7);
BW = f1-f0;
fc = (f0+f1)/2; % Center Frequency.
fAxis = (0:fR:nT/2*fR-fR)';
% Geo-Physical Model: Conversion to Electrical Model
disp('Converting Geo-Physical Model to Electrical Model ...');
[erLayer, Depth] = ComputeEr_General(geoData, nT, tS);
% Generating the Chirp Waveform.
disp('Generating Chirp Waveform ...');
[PMModel,AMModel] = Ampm_Model('');
[Wv] = GenerateAMPMChirpWaveform(radarParams,PMModel,AMModel);
% Compute Tx WvLen , and estimate initial order of Rng
f_t = linspace(f0,f1,length(tAxis))';
lam_t = (3e8/mean(f_t));
Rng = Depth(1); % Bcuz we are using equivalent gamma @ depth of
first layer.
disp('Computing Return Response ...');
R_Res = 3e8/(2*BW); % Range Resolution
Freq = 5e9; % Frequency to calculate scattering Power
79
Ht = Depth(1); % Ht of First interface.Depth
num_fft = 2^21;
disp('Compute Point Spread Function...');
%% Either PSF Can be Used!
%% PSF = ComputePSFromMeas(radarParams)';
PSF = ComputePSFromCalib(nT,'/rsl1/antarctica2003/sep28/snow2.1')';
disp('Computing Return due to Reflection...');
Gamma_Sc = ComputeGamma(radarParams,erLayer,Depth,tAxis,lam_t,[Sig_h_1,Sig_h_2])';
Rx_Sc = real(ifft(fft(Gamma_Sc).*fft(Wv)));
Rx_Wv = Rx_Sc.*lam_t./(2*Rng)/(4*pi);
IF1 = Wv.*Rx_Wv.*PSF;
IF1 = ApplyTimeWindow(IF1,win_type);
rProfile1 = 20*log10(abs(fftshift(fft(IF1,num_fft))/(length(Wv))));
rProfile1 = rProfile1(end/2+1:end);
power1 = 10.^(rProfile1/10);
disp('Computing Return due to Scattering...');
[Sigma0vsD, Dists] = ComputeSigma0(Freq, Ht, 1/tS, R_Res,Sig_h_1, Sig_h_2, L_c_1, L_c_2,geoData);
DvsT = ComputeDvsT(radarParams, erLayer, Depth);
Sigma0 = interp1(Dists,Sigma0vsD,DvsT,'',0)';
Sigma_1 = zeros(size(Sigma0));
Sigma_1(1:4:end) = Sigma0(1:4:end);
Num_Avg = 20;
rProfile2_avg = zeros(num_fft/2,1);
for indx_var = 1:Num_Avg
Sigma_ph = Sigma_1 .*exp(j*2*pi*rand(size(Sigma_1))); %% Phase
Fluctuations
Rx_s1 = real(ifft(fft(Sigma_ph).*fft(Wv)));
IF2 = Wv.*Rx_s1.*PSF;
IF2 = ApplyTimeWindow(IF2,win_type);
rProfile2 =20*log10(abs(fftshift(fft(IF2,num_fft))/(length(Wv))));
% rProfile2 = rProfile2(end/2+1:end);
rProfile2_avg = rProfile2_avg + rProfile2(end/2+1:end)/Num_Avg;
if mod(indx_var,10) == 0
disp(indx_var);
end;
end;
power2 = 10.^(rProfile2_avg/10);
% Computing the Distance Profile.
fr_Axis = 0:fS/num_fft:fS/2-fS/num_fft;
dProfile = ComputeDProfile(radarParams, erLayer, Depth, fr_Axis);
Noise_Dist = [0:R_Res:max(dProfile)];
80
Noise = mean(power1)*0.01*randn(size(Noise_Dist)); % -60dB Noise @
every range cell.
Noise_dProfile = interp1(Noise_Dist,Noise,dProfile)';
power1 = power1+Noise_dProfile; % Noise is added here ALONE!
rProfile1 = 10*log10(abs(power1));
power3 = power1+power2;
rProfile3 = 10*log10(abs(power3));
rProfile1 = rProfile1 - max(rProfile3);
rProfile2_avg = rProfile2_avg - max(rProfile3);
rProfile3 = rProfile3 - max(rProfile3);
% Profile Vs. Distance.
figure;
plot(dProfile,rProfile3-max(rProfile3),'r');
xlabel('Distance (m) -->');
ylabel('Magnitude (dB) -->');
title('Simulation of Return from Snow Over Sea Ice - Rough Surfaces');
axis([geoData(1,1)-1,geoData(1,1)+1.5,-100,0]);
grid on;