%% Determine Gamma
function [Gamm_long] = ComputeGamma(radarParams,
erLayer,Depth,tAxis,Wv_Len,Sigh)
erLayer_m = mean(erLayer);
Vel_m = 3e8./sqrt(erLayer_m);
Range = cumsum(Depth);
Dist = Range(1:end-1);
70
for index_var = 1:length(Depth)-1
Gamm(index_var) = (sqrt(erLayer_m(index_var+1)) -
sqrt(erLayer_m(index_var)))/...
(sqrt(erLayer_m(index_var+1)) +
sqrt(erLayer_m(index_var)));
end;
depths = ComputeDvsT(radarParams, erLayer, Depth);
Wv_Len_m = Wv_Len ./ sqrt(erLayer_m);
K = 2*pi./Wv_Len_m(1:end-1);
Gamm = Gamm.*exp(-4*K.^2.*Sigh.^2);
% Interpolate Gamma to all values!
Gamm_long = zeros(size(tAxis));
Gamm_long(max(find(depths < Dist(1)))) = Gamm(1);
Gamm_long(max(find(depths < Dist(2)))) = Gamm(2);
return;