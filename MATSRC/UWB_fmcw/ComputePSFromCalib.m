%% PSFromCalib.m
%% Takes in the calibration file
%% And Generates Transfer Function
function [PSF] = ComputePSFromCalib(nT,Fname1);
Num_Fft=2^19;
BW = 6e9;
T_swp = 10e-3;
N_swp = 50000;
fid1 = fopen(Fname1,'r');
%% Read in Headers
header1 = fread(fid1,3,'float64')';
header1 = [header1 fread(fid1,3,'int32')'];
header1 = [header1 fread(fid1,2,'float64')'];
numsam1 = header1(4);
Fs1 = header1(3); %Sampling Rate
F_ax1=-Fs1/2:Fs1/Num_Fft:Fs1/2-Fs1/Num_Fft;
F_ax1_p=F_ax1(end/2+1:end);
D_ax1=F_ax1_p*3e8*T_swp/(2*BW);
Ts1 = 1/Fs1;
fAxis = linspace(2e9,8e9,nT);
Cur_Pos1 = ftell(fid1);
No_AScopes1 = length(fread(fid1,inf,'int16'))/64006;
fseek(fid1,Cur_Pos1,-1);
%% AScope_Num = ceil(No_AScopes1);
AScope_Num = 5;
71
for i=1:No_AScopes1
Data1 = fread(fid1,numsam1,'int16');
Time1 = fread(fid1,3,'int32');
Upsweep1 = Data1(1:N_swp);
Data_Arr1(:,i) = Upsweep1;
end
fclose(fid1);
Calib_data = Data_Arr1(:,AScope_Num)/65536; % Normalize the Data
Calib_data = resample(Calib_data,nT,N_swp);
Calib_data_f = fftshift(fft(Calib_data',Num_Fft));
Calib_indx = find(Calib_data_f(end/2+1:end) ==
max(Calib_data_f(end/2+1:end)));
F_calib = F_ax1_p(Calib_indx);
Filter_Wn = [Calib_indx-50 Calib_indx+50]/Num_Fft;
[Filt_B,Filt_A] = butter(2,Filter_Wn);
[Filt_H,Filt_W] = freqz(Filt_B,Filt_A,Num_Fft);
Calib_data_filt_f = (ifftshift(Calib_data_f).*abs(Filt_H)')';
Calib_data_filt = real(ifft(Calib_data_filt_f'));
Calib_data_filt_m = hilbert(Calib_data_filt(1:nT));
System_m = abs(Calib_data_filt_m);
System_p = abs(unwrap(angle(Calib_data_filt_m))) -
2*pi*F_calib*T_swp*(0:nT-1)/nT;
System_func1 = System_m.*exp(j*System_p);
System_m = SysDivMag(System_m',2e9,8e9,'IdMag_50k.txt');
System_func2 = System_m'.*exp(j*System_p);
PSF = System_func1;
return;