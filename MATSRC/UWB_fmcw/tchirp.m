% test LFM-linear FM signal
% natural constants
c=3e8;

%generate transmitted chirp
dt=0.00001;
t=0:dt:1;
K=40; %linear FM rate
s=sin(pi*K*(t.*t));
figure(1);plot(t,s);

% plot spectrum of s
S=fft(s); 
S_MAG=abs(S.*conj(S));
figure(2);loglog(S_MAG);
% delayed chirp;
dl=1e8;
tau=dl/c;
%receive wave
% calculate delay in term of samples
Ntau=round(tau/dt);
Ns=length(s);
%r=zeros(1,Ns);
%r(Ntau:Ns)=s(1:Ns-Ntau+1);
r=sin(pi*K*((t-tau).*(t-tau)));

figure(3);plot(r);

%plot spectrum of r
R=fft(r); 
R_MAG=abs(R.*conj(R));
figure(4);loglog(R_MAG);
figure(5);plot(R_MAG);


%demodulate,
dem=r.*s;
%figure(4); plot(dem);
%display spectrum
DEM=fft(dem);
DEM_MAG=abs(DEM.*conj(DEM));
figure(5);loglog(DEM_MAG);

figure(10);semilogx(DEM_MAG);
figure(100);plot(DEM_MAG);
