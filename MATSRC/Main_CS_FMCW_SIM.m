% =========================================================
% Compressed-Sensing, Multirange-Resolution FHSS-FMCW radar
% Created by: Andriyan B. Suksmono
%  School of Elec. Eng. & Informatics/IRCTR-IB
%  Institut Teknologi Bandung, Indonesia
% Date: 20/January/2011
% =========================================================
% ?????? To be reformulated ???????
%
% Abstract: 
% 1) PNG generates random reference modulation frequency
% 2) sparsely selected carrier are transmitted and detected
% 3) beats are extracted from sparsed data
% 4) L1 construct all-object within the range 
%
% Features:
% 1) simultaneous construction of multirange echoes
% 2) encrypted/secured/ anti-jamm due to FHSS
% 3) less data by CS imaging
%
% Implementation:
% 1) prototyping on GNU-Radio/USRP2 kit
%
% Issues: doppler ?
%
% Natural constants
c=3e8; %the speed of light

% operational parameters
fc = 2.8E9; % the radar works at 2.8 GHz 
BW = 30E6 ; % radar bandwidth is 30 MHz;
% range cell number
N_cell = 1024; 
% range resolution
cell_res = c/(2*BW);
% maximum unambiguous range
max_range = N_cell*cell_res; % = 5120 meters
% sweep time = T = max_range / speed-of-light
T = max_range/c;
% FM rate K = Bandwidth/sweep_time
K=BW/T;
% discretization of time; dt = delay_time_of_max_range/(2*N_cell)

dt=T/(2*N_cell);
t=0:dt:T;

%generate chirp
s_chirp = sin(pi*K*(t.*t));

% ==================================================
% Baseband Processing Only, no carrier
% ==================================================
%generate transmitted chirp
s=s_chirp; 
figure(1);plot(t,s);
xlabel('time (s)'); ylabel('Amplitude (V)');
title('Chirp');

% plot spectrum of s
S=fft(s); 
S_MAG=abs(S);
df=1/T;
f=(0:df:2*BW-df);
figure(2); 
plot(f,S_MAG(1:floor(N_cell)));
title('Chirp spectrum');
xlabel('Frequency (Hz)'); ylabel('Magnitude');
%loglog(S_MAG(1:floor(N_cell)));

% simulate reflection
% position of target: 3Km ;
L= 3000;
tau=2*L/c;
%received chirp
r=sin(pi*K*((t-tau).*(t-tau)));
figure(3);plot(t,r); 
title('Received chirp');
xlabel('Time (s)'); ylabel('Amplitude');

%Create range-axis
x=cell_res*(0:N_cell-1);
%plot spectrum of r
R=fft(r); 
R_MAG=abs(R);
figure(10);
plot(x,R_MAG(1:floor(N_cell)));
title('Range spectrum');
xlabel('Range (m)'); ylabel('Magnitude');
%loglog(R_MAG(1:floor(N_cell)));
% 
%demodulate,
dem=r.*s;
% no-carrier -> no filtering -> get demodulated signal
f_beat=dem;
figure(11);plot(f_beat); title('beat signal unfiltered');

% display range-spectrum
F_BEAT=fft(f_beat);
F_BEAT_MAG=abs(F_BEAT);
%create range-axis
figure(20);
plot(x,F_BEAT_MAG(1:floor(N_cell)));
%plot(x,real(F_BEAT(1:floor(N_cell))));
xlabel('Range (m)'); ylabel('Magnitude');
title('Range Spectrum of Object at 3000 m');
% NEXT:
% -> multiple reflections
% -> carrier and noise
% -> match filtering 
% -> Doppler processing ?

%SFCW-like processing
sin_chirp = sin(pi*K*(t.*t));
cos_chirp = cos(pi*K*(t.*t));

%I/Q demodulation with the chirp and its quadrature
I=r.*sin_chirp;
Q=r.*cos_chirp;

%
g=ifft(I+i*Q);
%g=fft(sin_chirp+i*cos_chirp);
figure(31);plot(x,real(g(1:N_cell)));
xlabel('Range (m)'); ylabel('Amplitude');
grid on;

figure(32);plot(x,abs(g(1:N_cell)));
xlabel('Range (m)'); ylabel('Amplitude');title('Range Spectrum');
grid on;

xq=1:length(I);
figure(41);plot(xq,I,'b-',xq,Q,'r:');
legend('Inphase', 'Quadrature');title ('Beat Signal');


%figure(31);plot(x,abs(g(1:N_cell)));
%---a