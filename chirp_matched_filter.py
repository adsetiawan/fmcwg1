#!/usr/bin/env python
##################################################
# Gnuradio Python Flow Graph
# Title: Chirp Matched Filtering
# Author: Andriyan Suksmono
# Description: Matched Filtering Demo of LFM Signals
# Generated: Fri Apr 12 10:00:35 2013
##################################################

from gnuradio import eng_notation
from gnuradio import gr
from gnuradio import window
from gnuradio.eng_option import eng_option
from gnuradio.gr import firdes
from gnuradio.wxgui import fftsink2
from gnuradio.wxgui import scopesink2
from grc_gnuradio import wxgui as grc_wxgui
from optparse import OptionParser
import wx

class chirp_matched_filter(grc_wxgui.top_block_gui):

	def __init__(self):
		grc_wxgui.top_block_gui.__init__(self, title="Chirp Matched Filtering")
		_icon_path = "/usr/share/icons/hicolor/32x32/apps/gnuradio-grc.png"
		self.SetIcon(wx.Icon(_icon_path, wx.BITMAP_TYPE_ANY))

		##################################################
		# Variables
		##################################################
		self.samp_rate = samp_rate = 1000000

		##################################################
		# Blocks
		##################################################
		self.notebook_0 = self.notebook_0 = wx.Notebook(self.GetWin(), style=wx.NB_TOP)
		self.notebook_0.AddPage(grc_wxgui.Panel(self.notebook_0), "waveform")
		self.notebook_0.AddPage(grc_wxgui.Panel(self.notebook_0), "spectrum")
		self.notebook_0.AddPage(grc_wxgui.Panel(self.notebook_0), "matched-filter")
		self.Add(self.notebook_0)
		self.wxgui_scopesink2_0_0 = scopesink2.scope_sink_f(
			self.notebook_0.GetPage(2).GetWin(),
			title="Scope Plot",
			sample_rate=samp_rate,
			v_scale=0,
			v_offset=0,
			t_scale=0,
			ac_couple=False,
			xy_mode=False,
			num_inputs=1,
			trig_mode=gr.gr_TRIG_MODE_AUTO,
			y_axis_label="Counts",
		)
		self.notebook_0.GetPage(2).Add(self.wxgui_scopesink2_0_0.win)
		self.wxgui_scopesink2_0 = scopesink2.scope_sink_f(
			self.notebook_0.GetPage(0).GetWin(),
			title="Scope Plot",
			sample_rate=samp_rate,
			v_scale=0,
			v_offset=0,
			t_scale=0,
			ac_couple=False,
			xy_mode=False,
			num_inputs=1,
			trig_mode=gr.gr_TRIG_MODE_AUTO,
			y_axis_label="Counts",
		)
		self.notebook_0.GetPage(0).Add(self.wxgui_scopesink2_0.win)
		self.wxgui_fftsink2_0 = fftsink2.fft_sink_f(
			self.notebook_0.GetPage(1).GetWin(),
			baseband_freq=0,
			y_per_div=10,
			y_divs=10,
			ref_level=0,
			ref_scale=2.0,
			sample_rate=samp_rate,
			fft_size=1024,
			fft_rate=15,
			average=False,
			avg_alpha=None,
			title="FFT Plot",
			peak_hold=False,
		)
		self.notebook_0.GetPage(1).Add(self.wxgui_fftsink2_0.win)
		self.gr_vector_to_stream_0_0 = gr.vector_to_stream(gr.sizeof_gr_complex*1, 1024)
		self.gr_vector_to_stream_0 = gr.vector_to_stream(gr.sizeof_gr_complex*1, 1024)
		self.gr_vco_f_0 = gr.vco_f(samp_rate, 628000, 1)
		self.gr_throttle_0 = gr.throttle(gr.sizeof_float*1, samp_rate)
		self.gr_stream_to_vector_0_0 = gr.stream_to_vector(gr.sizeof_gr_complex*1, 1024)
		self.gr_stream_to_vector_0 = gr.stream_to_vector(gr.sizeof_float*1, 1024)
		self.gr_sig_source_x_0 = gr.sig_source_f(samp_rate, gr.GR_SAW_WAVE, 1000, 1, 0)
		self.gr_multiply_xx_0 = gr.multiply_vcc(1024)
		self.gr_integrate_xx_0 = gr.integrate_cc(1)
		self.gr_fft_vxx_0_0_0 = gr.fft_vcc(1024, False, (window.blackmanharris(512)), True, 1)
		self.gr_fft_vxx_0_0 = gr.fft_vfc(1024, True, (window.blackmanharris(512)), 1)
		self.gr_conjugate_cc_0 = gr.conjugate_cc()
		self.gr_complex_to_mag_squared_0 = gr.complex_to_mag_squared(1)

		##################################################
		# Connections
		##################################################
		self.connect((self.gr_sig_source_x_0, 0), (self.gr_vco_f_0, 0))
		self.connect((self.gr_vco_f_0, 0), (self.gr_throttle_0, 0))
		self.connect((self.gr_throttle_0, 0), (self.wxgui_scopesink2_0, 0))
		self.connect((self.gr_throttle_0, 0), (self.wxgui_fftsink2_0, 0))
		self.connect((self.gr_multiply_xx_0, 0), (self.gr_fft_vxx_0_0_0, 0))
		self.connect((self.gr_throttle_0, 0), (self.gr_stream_to_vector_0, 0))
		self.connect((self.gr_stream_to_vector_0, 0), (self.gr_fft_vxx_0_0, 0))
		self.connect((self.gr_fft_vxx_0_0, 0), (self.gr_multiply_xx_0, 0))
		self.connect((self.gr_fft_vxx_0_0, 0), (self.gr_vector_to_stream_0, 0))
		self.connect((self.gr_vector_to_stream_0, 0), (self.gr_conjugate_cc_0, 0))
		self.connect((self.gr_conjugate_cc_0, 0), (self.gr_stream_to_vector_0_0, 0))
		self.connect((self.gr_stream_to_vector_0_0, 0), (self.gr_multiply_xx_0, 1))
		self.connect((self.gr_fft_vxx_0_0_0, 0), (self.gr_vector_to_stream_0_0, 0))
		self.connect((self.gr_vector_to_stream_0_0, 0), (self.gr_integrate_xx_0, 0))
		self.connect((self.gr_integrate_xx_0, 0), (self.gr_complex_to_mag_squared_0, 0))
		self.connect((self.gr_complex_to_mag_squared_0, 0), (self.wxgui_scopesink2_0_0, 0))

	def get_samp_rate(self):
		return self.samp_rate

	def set_samp_rate(self, samp_rate):
		self.samp_rate = samp_rate
		self.wxgui_fftsink2_0.set_sample_rate(self.samp_rate)
		self.wxgui_scopesink2_0.set_sample_rate(self.samp_rate)
		self.gr_sig_source_x_0.set_sampling_freq(self.samp_rate)
		self.wxgui_scopesink2_0_0.set_sample_rate(self.samp_rate)

if __name__ == '__main__':
	parser = OptionParser(option_class=eng_option, usage="%prog: [options]")
	(options, args) = parser.parse_args()
	tb = chirp_matched_filter()
	tb.Run(True)

